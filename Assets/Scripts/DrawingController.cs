﻿using UnityEngine;
using UnityEngine.EventSystems;
using System.Collections;
using System.Collections.Generic;

public class DrawingController : MonoBehaviour, IPointerDownHandler, IPointerUpHandler, IDragHandler
{
	public float _drawingLineWidth = 0.2f;
	public float _drawnLineWidth = 1f;

	public float _snapDistanceThreshold = 0.25f;

	[SerializeField] private LineRenderer _lineRendererPrefab;

	private List<LineRenderer> _lines;
	private float _scale;
	private bool _mouseDown = false;

	private LineRenderer _currentDrawingLine;

	public void Initialize (float scale) {
		_scale = scale;

		_lines = new List<LineRenderer> ();
	}

	public void OnPointerDown (PointerEventData pointerData) {
		InstantiateCurrentLine ();

		SetCurrentLineWidth (_drawingLineWidth);

		_mouseDown = true;
	}

	public void OnDrag (PointerEventData pointerData) {
		if (_mouseDown == true) {
			//-- Snap to grid here.
//			Debug.Log("pointerData.position: " + Camera.main.ScreenToWorldPoint (pointerData.position));
//			Debug.Log("pointerData.position: " + pointerData.position);
			Vector2 lineEndPoint = SnapToGrid(pointerData.position);
			Debug.Log("lineEndPoint: " + lineEndPoint);
			DrawLine (pointerData.pressPosition, lineEndPoint, _currentDrawingLine);
		}
	}

	public void OnPointerUp (PointerEventData pointerData) {
		//-- Snap to grid here.
		Vector2 lineEndPoint = SnapToGrid(pointerData.position);
		DrawLine (pointerData.pressPosition, lineEndPoint, _currentDrawingLine);

		SetCurrentLineWidth (_drawnLineWidth);

		_mouseDown = false;
		_lines.Add (_currentDrawingLine);
		_currentDrawingLine = null;
	}

	private void InstantiateCurrentLine () {
		_currentDrawingLine = Instantiate (_lineRendererPrefab, this.transform) as LineRenderer;
		_currentDrawingLine.transform.position = Vector2.zero;
	}

	private void DrawLine (Vector2 startPosition, Vector2 endPosition, LineRenderer lineRenderer) {
		startPosition = Camera.main.ScreenToWorldPoint (startPosition);
//		endPosition = Camera.main.ScreenToWorldPoint (endPosition);

		lineRenderer.SetPosition (0, startPosition);
		lineRenderer.SetPosition (1, endPosition);
	}

	private void SetCurrentLineWidth (float width) {
		_currentDrawingLine.startWidth = width;
		_currentDrawingLine.endWidth = width;
	}

	private Vector2 SnapToGrid (Vector2 lineEndPoint) {
		Vector2 snapPoint = Camera.main.ScreenToWorldPoint (lineEndPoint);
//		Vector2 snapPoint = lineEndPoint;

		int xCount = Mathf.RoundToInt(snapPoint.x / _scale);
		int yCount = Mathf.RoundToInt(snapPoint.y / _scale);

		Vector2 result = new Vector2 ((float)xCount * _scale, (float)yCount * _scale);

		Debug.Log ("snapPoint: " + snapPoint);
		Debug.Log ("result: " + result);

		float xDistance = Mathf.Abs (result.x - snapPoint.x);
		if (xDistance < _snapDistanceThreshold) {
			snapPoint = new Vector2 (result.x, snapPoint.y);
		}
		Debug.Log ("xDistance: " + xDistance);

		float yDistance = Mathf.Abs (result.y - snapPoint.y);
		if (yDistance < _snapDistanceThreshold) {
			snapPoint = new Vector2 (snapPoint.x, result.y);
		}
		Debug.Log ("yDistance: " + yDistance);

		return snapPoint;
	}
}