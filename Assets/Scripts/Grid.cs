﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Grid : MonoBehaviour {
	public Vector2[,] _gridPoints;

	public void SetGridPoints (Vector2[,] gridPoints) {
		_gridPoints = gridPoints;
	}
}