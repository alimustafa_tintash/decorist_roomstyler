﻿using UnityEngine;
using System.Collections;
using System;

public class GridCreator : MonoBehaviour
{
	[SerializeField] public int _xSize;
	[SerializeField] private int _ySize;

	private Vector2[,] _gridPoints;

	public Vector2[,] GenerateGridPoints (Vector2 startPosition) {
		Vector2 [,] gridPoints;

		gridPoints = new Vector2[(_xSize + 1), (_ySize + 1)];

		for (int i = 0; i < _ySize; i++) {
			for (int j = 0; j < _xSize; j++) {
				gridPoints [i, j] = new Vector2 (startPosition.x+i, startPosition.y+j);
			}
		}

		_gridPoints = gridPoints;

		return gridPoints;
	}

	private void OnDrawGizmos () {
		if (_gridPoints == null) {
			return;
		}

		Gizmos.color = Color.black;
		for (int i = 0; i < _ySize; i++) {
			for (int j = 0; j < _xSize; j++) {
				Gizmos.DrawSphere(_gridPoints [i, j], 0.1f);
			}
		}
	}
}

