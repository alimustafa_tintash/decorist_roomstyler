﻿using UnityEngine;
using System.Collections;

public class MainController : MonoBehaviour
{
	[SerializeField] private float _scale = 1f;

	[Tooltip("The plane on which the grid points are calculated.")]
	[SerializeField] private GameObject _gridPlane;

	private GridCreator _gridCreator;
	private Grid _grid;
	private DrawingController _drawingController;

	void Awake () {
		_gridCreator = GetComponent<GridCreator> ();
		_grid = gameObject.AddComponent<Grid> ();
		_grid._gridPoints = _gridCreator.GenerateGridPoints (Camera.main.ScreenToWorldPoint (_gridPlane.transform.position));

		_drawingController = GetComponentInChildren<DrawingController> ();
	}

	void Start () {
		_drawingController.Initialize (_scale);
	}
}

